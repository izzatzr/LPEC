# LPeC

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```

### Compiles and minifies for production

```bash
npm run build
```

## TODO List

* Use firebase functions as a back end
* Convert to typescript
* Dynamic Layout
* Loading Page
* Vuex (?)
* Make as general CAT
* etc..._
