import Vue from 'vue'
import Router from 'vue-router'
// import firebase from 'firebase'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/portal',
      component: () => import('@/views/Portal.vue'), // default root page
    },
    {
      path: '/portal',
      name: 'portal',
      component: () => import('@/views/Portal.vue')
    },
    {
      path: '/files',
      name: 'files',
      component: () => import('@/views/Files.vue')
    },
    {
      path: '/score',
      name: 'score',
      component: () => import('@/views/Exam/Score.vue'),
      meta: {
        protected: true
      }
    },
    {
      path: '/exam',
      name: 'exam',
      component: () => import('@/views/Exam/Index.vue'),
      meta: {
        protected: true
      }
    },
    {
      path: '/exam/start',
      name: 'examstart',
      component: () => import('@/views/Exam/Start.vue'),
      meta: {
        protected: true
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('@/views/Admin/Index.vue'),
      meta: {
        protected: true
      }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/About/Index.vue')
    },

  ]
})
