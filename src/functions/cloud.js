import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyATeh2itVB07yUFKHWkxbiUfnfe8d7aA9s",
    authDomain: "asop-cc502.firebaseapp.com",
    databaseURL: "https://asop-cc502.firebaseio.com",
    projectId: "asop-cc502",
    storageBucket: "asop-cc502.appspot.com",
    messagingSenderId: "503722876031"
});

export default firebase