import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import firebase from './functions/cloud'
import './plugins/vuetify'
import firebase from 'firebase'

firebase.initializeApp({
  apiKey: "AIzaSyATeh2itVB07yUFKHWkxbiUfnfe8d7aA9s",
  authDomain: "asop-cc502.firebaseapp.com",
  databaseURL: "https://asop-cc502.firebaseio.com",
  projectId: "asop-cc502",
  storageBucket: "asop-cc502.appspot.com",
  messagingSenderId: "503722876031"
});


Vue.config.productionTip = false
const top_secret = "UmV7751QW5MPzhu8HvAH4BaXz3h1";


router.beforeEach((to, from, next) => {
  firebase.auth().onAuthStateChanged(user => {
    switch (to.fullPath) {
      case '/admin':
        if (user.uid != top_secret) router.push('/portal')
        break;
      case '/portal':
        if (!user) break;
        if (user.uid == top_secret) router.replace('/admin')
        else if (user.uid != top_secret) router.replace('/exam')
        break;
      default:
        if (to.meta.protected) {
          if (!user) router.replace('/portal')
        }
        break;
    }
    next();
  })
})

new Vue({
  router,
  render: h => h(App),
  el: '#app'
})
